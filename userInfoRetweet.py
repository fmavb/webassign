from pymongo import MongoClient, errors
import re

client = MongoClient('mongodb://127.0.0.1:27017')
db = client.assignment

data = db.combined.find()
userGraph = {}

for tweet in data:
    if 'retweeted_status' in tweet and 'quoted_status' not in tweet:
        user = tweet['user']['screen_name']
        mentions = tweet['entities']['user_mentions']
        for mention in mentions:
            if user in userGraph:
                if mention['screen_name'] in userGraph[user]:
                    userGraph[user][mention['screen_name']] += 1
                else:
                    userGraph[user][mention['screen_name']] = 1
                
            else:
                userGraph[user] = {}
                userGraph[user][mention['screen_name']] = 1

db.generalGraphRetweet.insert(userGraph)
