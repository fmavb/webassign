from pymongo import MongoClient


client = MongoClient('mongodb://127.0.0.1:27017')
db=client.assignment
serverStatusResult=db.command("serverStatus")
print(serverStatusResult)

data = db.streaming.find()
valueCount = {}

for elt in data:
    if elt['user']['id'] in valueCount:
        valueCount[elt['user']['id']] += 1
    else:
        valueCount[elt['user']['id']] = 1


for key, value in valueCount.items():
    db.powerUsers.insert_one({"user": key, "tweets": value})
