import networkx as nx
from pymongo import MongoClient
import matplotlib.pyplot as plt

client = MongoClient('mongodb://127.0.0.1:27017')
db = client.assignment

graph = nx.DiGraph()

users = db.generalGraphMention.find()[0]

for key, values in users.items():
    if key == '_id':
        continue
    for key1 in values.keys():
        graph.add_edge(key, key1)


print(nx.algorithms.triads.triadic_census(graph))

#nx.draw(graph, node_size=1)
#plt.savefig("graph2.png")