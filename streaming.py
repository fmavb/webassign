# Obtained from: https://medium.com/@jaimezornoza/downloading-data-from-twitter-using-the-streaming-api-3ac6766ba96c

#Import the necessary methods from tweepy library
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from pymongo import MongoClient
from pymongo import errors
import json
from datetime import datetime

# Enter Twitter API Keys
access_token = "XXXXXXXXXXXXXXXXXXXXXXXXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
access_token_secret = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
consumer_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
consumer_secret = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

client = MongoClient('mongodb://127.0.0.1:27017')
db=client.assignment
serverStatusResult=db.command("serverStatus")
print(serverStatusResult)


# Create the class that will handle the tweet stream
class StdOutListener(StreamListener):
      
    def on_data(self, data):
        data = json.loads(data)
        print(data)
        print('\n')
        if "created_at" in data:
            dt = data["created_at"]
            created = datetime.strptime(dt, '%a %b %d %H:%M:%S +0000 %Y')
            data["created_at"] = created
            data['_id'] = data['id']
            if data["lang"] != "en":
                return True
            try:
                db.streaming.insert_one(data)
            except(errors.DuplicateKeyError):
                return True
        return True
        

    def on_error(self, status):
        print(status)


if __name__ == '__main__':  
# Handle Twitter authentication and the connection to Twitter Streaming API
    l = StdOutListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    stream = Stream(auth, l)
    stream.sample()
    
    print (db.streaming.find().count())
    print ("Completed")