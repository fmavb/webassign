from pymongo import MongoClient
import spacy
import json

client = MongoClient('mongodb://127.0.0.1:27017')
db = client.assignment


nlp = spacy.load('en_core_web_sm', disable=['ner'])
nlp.remove_pipe('tagger')
nlp.remove_pipe('parser')
spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS

#@Tokenize
def spacy_tokenize(string):
  tokens = list()
  doc = nlp(string)
  for token in doc:
    tokens.append(token)
  return tokens

#@Normalize
def normalize(tokens):
  normalized_tokens = list()
  for token in tokens:
    if (not token.is_punct):
#    if (token.is_alpha or token.is_digit):
      normalized = token.text.lower().strip()
      if normalized not in spacy_stopwords:
        normalized_tokens.append(normalized)
  return normalized_tokens

#@Tokenize and normalize
def tokenize_normalize(string):
  return normalize(spacy_tokenize(string))  

clusters = db.clustering.find()
output = {}

for elt in clusters:
    tokens = tokenize_normalize(elt['tweet'])
    if elt["label"] not in output:
        output[elt["label"]] = {}
    for token in tokens:
        if token in output[elt["label"]]:
            output[elt["label"]][token] += 1
        else:
            output[elt["label"]][token] = 1

for key, value in output.items():
    value = {k: v for k, v in sorted(value.items(), key=lambda item: item[1], reverse=True)}
    output[key] = value


temp = {}

for key, value in output.items():
    count = 0
    array = []
    for key1, value1 in value.items():
        if count < 10:
            array.append({key1: value1})
            count += 1
        else:
            break
    temp[key] = array

outputFile = open("wordCount.json", "w")

json.dump(temp, outputFile)

outputFile.close()