from pymongo import MongoClient

client = MongoClient('mongodb://127.0.0.1:27017')
db=client.assignment

data = db.combined.find()

mentions = {}

for tweet in data:
    if 'retweeted_status' not in tweet and 'quoted_status' in tweet:
        hashtags = tweet['entities']['hashtags']
        for hashtag1 in hashtags:
            for hashtag2 in hashtags:
                if hashtag1['text'] == hashtag2['text']:
                    continue
                else:
                    if hashtag1['text'] in mentions:
                        if hashtag2['text'] in mentions[hashtag1['text']]:
                            mentions[hashtag1['text']][hashtag2['text']] += 1
                        else:
                            mentions[hashtag1['text']][hashtag2['text']] = 1
                    else:
                        mentions[hashtag1['text']] = {}
                        mentions[hashtag1['text']][hashtag2['text']] = 1

db.hashtagsGeneralME.insert(mentions)