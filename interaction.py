from pymongo import MongoClient

client = MongoClient('mongodb://127.0.0.1:27017')
db = client.assignment

data = db.combined.find()

for elt in data:
    if 'retweeted_status' not in elt:
        print (elt)
        break