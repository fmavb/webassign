from pymongo import MongoClient
import json

client = MongoClient('mongodb://127.0.0.1:27017')
db = client.assignment


clusters = db.clustering.find()
output = {}


for elt in clusters:
    tweet = db.combined.find({"_id": elt['_id']})[0]
    if elt["label"] not in output:
        output[elt["label"]] = {}
    for i in range(len(tweet['entities']['user_mentions'])):
        if tweet['entities']['user_mentions'][i]['screen_name'] in output[elt["label"]]:
            output[elt["label"]][tweet['entities']['user_mentions'][i]['screen_name']] += 1
        else:
            output[elt["label"]][tweet['entities']['user_mentions'][i]['screen_name']] = 1

for key, value in output.items():
    value = {k: v for k, v in sorted(value.items(), key=lambda item: item[1], reverse=True)}
    output[key] = value


temp = {}

for key, value in output.items():
    count = 0
    array = []
    for key1, value1 in value.items():
        if count < 10:
            array.append({key1: value1})
            count += 1
        else:
            break
    temp[key] = array

outputFile = open("mentionCount.txt", "w")

for key, value in temp.items():
    outputFile.write(str(key)+"\n\n")
    for elt in value:
        for key1, value1 in elt.items():
            outputFile.write("\"" + str(key1) + "\"" +": " + str(value1)+ "," + "\n")
    outputFile.write("\n")

outputFile.close()