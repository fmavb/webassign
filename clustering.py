from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
from pymongo import MongoClient, errors
import spacy

nlp = spacy.load('en_core_web_sm', disable=['ner'])
nlp.remove_pipe('tagger')
nlp.remove_pipe('parser')
spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS

#@Tokenize
def spacy_tokenize(string):
  tokens = list()
  doc = nlp(string)
  for token in doc:
    tokens.append(token)
  return tokens

#@Normalize
def normalize(tokens):
  normalized_tokens = list()
  for token in tokens:
    if (not token.is_punct):
#    if (token.is_alpha or token.is_digit):
        normalized = token.text.lower().strip()
        if normalized not in spacy_stopwords:
            normalized_tokens.append(normalized)
  return normalized_tokens

#@Tokenize and normalize
def tokenize_normalize(string):
  return normalize(spacy_tokenize(string))  

client = MongoClient('mongodb://127.0.0.1:27017')
db = client.assignment

alg = KMeans(n_clusters=10, n_jobs=-1)

streaming = db.streaming.find()
rest = db.rest.find()

tweetData = []
tweetIds = []

for tweet in streaming:
    tweetData.append(tweet['text'])
    tweetIds.append(tweet['_id'])

for tweet in rest:
    tweetData.append(tweet['text'])
    tweetIds.append(tweet['_id'])

vectoriser = TfidfVectorizer(tokenizer=tokenize_normalize)

tfIdf = vectoriser.fit_transform(tweetData)
print(vectoriser.get_feature_names())

print (tfIdf.shape)

predictions = alg.fit_predict(tfIdf)

combined = []

for i in range(len(predictions)):
    toAppend = []
    toAppend.append(tweetData[i].strip('\n'))
    toAppend.append(predictions[i])
    combined.append(toAppend)

combined = sorted(combined, key=lambda  x: x[1])


output = open("clustering.txt", "w")

for i in range(len(combined)):
    try:
        db.clustering.insert_one({'_id': tweetIds[i], "tweet": str(combined[i][0]), "label": int(combined[i][1])})
    except (errors.DuplicateKeyError):
        continue
    output.write(str(tweetIds[i]) + " " + str(combined[i][0]) + " " + str(combined[i][1]) + "\n")

output.close()

print ("Completed")