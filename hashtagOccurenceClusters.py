from pymongo import MongoClient

client = MongoClient('mongodb://127.0.0.1:27017')
db=client.assignment



for i in range(10):
    mentions = {}
    cluster = db.clustering.find({"label": i})
    for elt in cluster:
        tweet = db.combined.find({"_id": elt["_id"]})[0]
        if 'retweeted_status' not in tweet and 'quoted_status' in tweet:
            hashtags = tweet['entities']['hashtags']
            for hashtag1 in hashtags:
                for hashtag2 in hashtags:
                    if hashtag1['text'] == hashtag2['text']:
                        continue
                    else:
                        if hashtag1['text'] in mentions:
                            if hashtag2['text'] in mentions[hashtag1['text']]:
                                mentions[hashtag1['text']][hashtag2['text']] += 1
                            else:
                                mentions[hashtag1['text']][hashtag2['text']] = 1
                        else:
                            mentions[hashtag1['text']] = {}
                            mentions[hashtag1['text']][hashtag2['text']] = 1
    db.hashtagsClusterME.insert_one({"label": i, "dict": mentions})