from pymongo import MongoClient, errors
import json

client = MongoClient('mongodb://127.0.0.1:27017')
db = client.assignment

streaming = db.streaming.find()
rest = db.rest.find()

for elt in streaming:
    db.combined.insert_one(elt)

for elt in rest:
    elt["_id"] = elt["id"]
    try:
        db.combined.insert_one(elt)
    except (errors.DuplicateKeyError):
        continue