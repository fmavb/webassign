from pymongo import MongoClient

client = MongoClient('mongodb://127.0.0.1:27017')
db = client.assignment

graph = db.hashtagsClusterME.find()

interDict = {}
for i in range(10):
    interDict[i] = {}
    for key, value in graph[i]['dict'].items():
        if key != '_id':
            print (value)
            for key1, value1 in value.items():
                if (str(key1) + " => " + str(key)) not in interDict[i]:
                    interDict[i][str(key) + " => " + str(key1)] = value1

for key, value in interDict.items():
    value = {k: v for k, v in sorted(value.items(), key=lambda item: item[1], reverse=True)}
    interDict[key] = value


temp = {}

for key, value in interDict.items():
    count = 0
    array = []
    for key1, value1 in value.items():
        if count < 10:
            array.append({key1: value1})
            count += 1
        else:
            break
    temp[key] = array

outputFile = open("hashtagClusterME.txt", "w")

for key, value in temp.items():
    outputFile.write(str(key)+"\n\n")
    for elt in value:
        for key1, value1 in elt.items():
            outputFile.write("\"" + str(key1) + "\"" +": " + str(value1) + "\n")
    outputFile.write("\n")

outputFile.close()


    