from pymongo import MongoClient

client = MongoClient('mongodb://127.0.0.1:27017')
db = client.assignment


for i in range(10):
    userGraph = {}
    cluster = db.clustering.find({"label": i})
    for elt in cluster:
        tweet = db.combined.find({"_id": elt["_id"]})[0]
        if 'retweeted_status' not in tweet and 'quoted_status' in tweet:
            user = tweet['user']['screen_name']
        
            mentions = tweet['entities']['user_mentions']
            
            for mention in mentions:
                if user in userGraph:
                    if mention['screen_name'] in userGraph[user]:
                        userGraph[user][mention['screen_name']] += 1
                    else:
                        userGraph[user][mention['screen_name']] = 1
                
                else:
                    userGraph[user] = {}
                    userGraph[user][mention['screen_name']] = 1
    db.clusterMentionsME.insert_one({"label": i, "dict": userGraph})