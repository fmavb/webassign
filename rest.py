import tweepy
import json
from pymongo import MongoClient, errors
from datetime import datetime

access_token = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX-XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
access_token_secret = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
consumer_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
consumer_secret = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

client = MongoClient('mongodb://127.0.0.1:27017')
db = client.assignment

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth_handler=auth)


usersToFetch = db.powerUsers.find().sort("tweets", -1)[:20]
trendsToFetch = db.trends.find().sort("count",-1)[:20]

###print(api.search(q=trendsToFetch[0])[0]._json)

for elt in (usersToFetch):
    try:
        for el in tweepy.Cursor(api.user_timeline, id=elt['user']).items(100):
            data = el._json
            if "created_at" in data and data["lang"] == "en":
                dt = data["created_at"]
                created = datetime.strptime(dt, '%a %b %d %H:%M:%S +0000 %Y')
                data["created_at"] = created
                data['_id'] = data['id']
                try:
                    db.rest.insert_one(data)
                except(errors.DuplicateKeyError):
                    continue
    except:
        continue

for elt in trendsToFetch:
    for el in tweepy.Cursor(api.search, q=elt["tag"]).items(100):
        data = el._json
        if "created_at" in data and data["lang"] == "en":
            dt = data["created_at"]
            created = datetime.strptime(dt, '%a %b %d %H:%M:%S +0000 %Y')
            data["created_at"] = created
            data['_id'] = data['id']
            try:
                db.rest.insert_one(data)
            except(errors.DuplicateKeyError):
                continue

print ("Completed!")