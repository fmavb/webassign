from pymongo import MongoClient

client = MongoClient('mongodb://127.0.0.1:27017')
db = client.assignment

graph = db.hashtagsGeneralME.find()[0]

interDict = {}

for key, value in graph.items():
    if key != '_id':
        for key1, value1 in value.items():
            if (str(key1) + " => " + str(key)) not in interDict:
                interDict[str(key) + " => " + str(key1)] = value1

array = []
for k, v in sorted(interDict.items(), key=lambda item: item[1], reverse=True):
    array.append(k + ": " + str(v))

outputFile = open("hashtagsGeneralME.txt", "w")

for i in range(100):
    outputFile.write(array[i] + "\n")

outputFile.close()


    