# Twitter Crawler and information analyser

streaming.py and rest.py contain code required to run the streaming and rest data collection.

clustering.py contains the algorithm used to compute clusters.

userInfo.py and clusteringPropertiesUsers.py contains code to count user occurrences.
hashtagOccurence.py and hashtagOccurenceClusters.py contains code to count hashtags occurrence.
Files ending with sorting.py are used to sort the output.

userGraph.py contains code used to generate graphs and triads outputs.