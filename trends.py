from pymongo import MongoClient

client = MongoClient('mongodb://127.0.0.1:27017')
db=client.assignment
serverStatusResult=db.command("serverStatus")
print(serverStatusResult)

data = db.streaming.find()
valueCount = {}

for elt in data:
    body = elt['text']
    hashtags = elt['entities']['hashtags']
    for tag in hashtags:
        if tag['text'] in valueCount:
            valueCount[tag['text']] += 1
        else:
            valueCount[tag['text']] = 1

for key, value in valueCount.items():
    db.trends.insert_one({"tag":key, "count": value})
